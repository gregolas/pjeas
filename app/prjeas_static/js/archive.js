var ids = ['navissue', 'navall', 'navchina', 'navjapan', 'navtaiwan', 'navkorea', 'navother'];
var strings = {'navissue': 'All Issues',
               'navall': 'All',
               'navchina': 'China',
               'navjapan': 'Japan',
               'navtaiwan': 'Taiwan',
               'navkorea': 'Korea',
               'navother': 'Other'}
var lastreq = '';
function go(type) {
    var doc = document.getElementById("articles");
    var request = new XMLHttpRequest();
    var reqstr = "/prjeas/load/" + type + "";
    var t = type;
    var id = "nav" + type;
    var nav = document.getElementById(id);
    if (nav) {
        nav.innerHTML = '<strong><i class="fa fa-angle-right"></i> ' + strings[id]+'</strong>';
    }
    for (var s in ids) {
        if (ids[s] == id)
            continue;
        nav = document.getElementById(ids[s]);
        nav.innerHTML = strings[ids[s]];
    }
    request.open("GET", reqstr, true);
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) 
            if (request.responseText) {
                doc.innerHTML = request.responseText;  //? 
                lastreq = t;
            }
    };
    if (t!= lastreq) {
        request.send();
        doc.innerHTML = '<i class="myspin fa fa-spinner fa-spin fa-2x"></i>';
    }
}
