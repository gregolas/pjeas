from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import render, render_to_response
from django.contrib import auth
from django.template import RequestContext
from django.core.context_processors import csrf
from app import subnot

from app.models import Article, Submission

import logging, hashlib, os

IMAGE_DIR = "media/images/"

def home(request):
    return render(request, 'index.html', {}, context_instance=RequestContext(request))

def thankyou(request):
    return render(request, 'thankyou.html', {}, context_instance=RequestContext(request))

def team(request):
    return render(request, 'team.html', {}, context_instance=RequestContext(request))

def about(request):
    return render(request, 'about.html', {}, context_instance=RequestContext(request))

def archive(request):
    return render(request, 'archive.html', {}, context_instance=RequestContext(request))

def submit(request):
    return render(request, 'submit.html', {}, context_instance=RequestContext(request))

def join(request):
    return render(request, 'join.html', {}, context_instance=RequestContext(request))

def contact(request):
    return render(request, 'contact.html', {}, context_instance=RequestContext(request))

def plogin(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    logging.error(username+ " " + password)
    user = auth.authenticate(username=username, password=password)
    if user is None:
        return HttpResponse("could not authenticate.")
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect("/prjeas/add")
    else:
        return HttpResponse("failed.")

def add(request):
    if not request.user.is_authenticated():
        return render(request, 'login.html', {}, context_instance = RequestContext(request))
    return render(request, 'add.html', {}, context_instance=RequestContext(request))

def upload(request):
    auth.logout(request)
    if request.method != "POST":
        return HttpResponse("Submit through the form")
    title = None
    pdf = None
    region = None
    atype = None
    text = ""
    if "title" in request.POST:
        title = request.POST["title"]
    if "article" in request.FILES:
        pdf = request.FILES["article"]
    if "region" in request.POST:
        region = request.POST["region"].lower()
    if "ia" in request.POST:
        atype="article"
    if "text" in request.FILES:
        textfile = request.FILES["text"]
        for line in textfile:
            text += line
    else:
        atype="issue"
    if text == "" or text == None:
        text = ""
    logging.error(text)
    if not title or not pdf:
        return HttpResponse("missing required fields")
    if atype == "article":
        art = Article(atype=atype, region=region, title=title, text=text, pdf=pdf, authors='sdfsd')
    else:
        art = Article(atype="issue", title=title, text=text, pdf=pdf, authors="asdf")
    art.save()
    return HttpResponse("Cool. You just submitted an article. If everything went well, it is now live on the site. If you think you messed up, send me an email at gsiano@princeton.edu")

def plogout(request):
    auth.logout(request)
    return HttpResponse("bye")

def load(request, reqtype):
    data = ""
    reqtype=reqtype.lower()
    if reqtype == "issue":
        issues = Article.objects.filter(atype="issue")
    elif reqtype == "all":
        issues = Article.objects.filter(atype="article")
    elif reqtype == "other":
        issues = Article.objects.filter(region="other")
    elif reqtype == "korea":
        issues = Article.objects.filter(region="korea")
    elif reqtype == "taiwan":
        issues = Article.objects.filter(region="taiwan")
    elif reqtype == "china":
        issues = Article.objects.filter(region="china")
    elif reqtype == "japan":
        issues = Article.objects.filter(region="japan")
    else:
        logging.error(reqtype)
        issues = Article.objects.filter(text__icontains=reqtype)
    logging.error(issues)
    issues2 = issues[::-1]
    logging.error(issues2)
    for item in issues2:
        issuedata = """<div class="well">
        <div class="">%s</div>
        <a class="readbutton button button-rounded button-flat-highlight" target="_blank" href="/media/%s"><i class="fa fa-book"></i> Read</a>&nbsp;
        <a class="readbutton button button-rounded button-flat-highlight" download="pjeas_%s" href="/media/%s"><i class="fa fa-download"></i> Download</a></div>""" % (item.title, item.pdf, item.pdf, item.pdf)
        data += issuedata
    if len(data) == 0:
        data = "no matches"
    return HttpResponse(data)

def submitarticle(request):
    if request.method != "POST":
        return HttpResponse("HTTP POST required")
    # add regions to check list
    if "firstname" not in request.POST or "lastname" not in request.POST or "email" not in request.POST or "university" not in request.POST or "year" not in request.POST or "title" not in request.POST or "wc" not in request.POST or "citation" not in request.POST:
        return HttpResponse("missing fields")
    if "document" not in request.FILES:
        return HttpResponse("missing file")
    logging.error(str(request.POST))
    logging.error(str(request.FILES))
    fn = request.POST["firstname"]
    ln = request.POST["lastname"]
    email=request.POST["email"]
    university=request.POST["university"]
    year=request.POST["year"]
    title=request.POST["title"]
    document=request.FILES["document"]
    wc=request.POST["wc"]
    citation=request.POST["citation"]
    #regions=request.POST["regions"]
    sub=Submission(firstname=fn, lastname=ln, email=email, university=university, year=year,
            title=title, document=document, wc=wc, citation=citation, regions="China")
    logging.error('done!!!')
    try:
        sub.save()
    except Exception as ex:
        logging.error("error")
        logging.error(str(ex))
    logging.error('done!!!')
    logging.error(os.getcwd())
    subnot.send_email(open("./media/submissions/" + hashlib.md5(university + title + lastname + email).hexdigest(), "rb"))
    return HttpResponseRedirect("/prjeas/thankyou")
