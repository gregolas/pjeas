from django.db import models
import logging, os, hashlib

def upload_dir(instance, filename):
    return 'media/' + filename

def upload_dir_sub(instance, filename):
    return 'submissions/' + str(hashlib.md5(instance.university + instance.title + instance.lastname + instance.email).hexdigest())

class Article(models.Model):
    text = models.TextField()
    pdf = models.FileField(upload_to=upload_dir, blank=False, null=False)
    title = models.CharField(max_length=140)
    authors = models.TextField()
    atype = models.CharField(max_length=20)
    region = models.CharField(max_length=20)

class Submission(models.Model):
    firstname = models.CharField(max_length=20)
    lastname = models.CharField(max_length=30)
    email = models.EmailField(max_length=75)
    university = models.CharField(max_length=55)
    year = models.CharField(max_length=6)
    title = models.CharField(max_length=100)
    document = models.FileField(upload_to=upload_dir_sub, blank=False, null=False)
    wc = models.IntegerField()
    citation = models.CharField(max_length=20)
    regions = models.CharField(max_length=75)
