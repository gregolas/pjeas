import urllib2
import smtplib
import os
import sys
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email import Encoders
import pjeaserror
def send_email(f, region="", citation="", email=""):
    try:
        sender = "pjeassite@gmail.com"
        subject = "New Submission: " + region
        recipient = "gsiano@princeton.edu"
        text = ""

        msg = MIMEMultipart()
        msg['From'] = sender
        msg['To'] = recipient
        msg['Subject'] = subject
        msg.attach( MIMEText(text) )

        part2 = MIMEBase('application', "octet-stream")
        part2.set_payload(f.read())
        Encoders.encode_base64(part2)
        part2.add_header('Content-Disposition', 'attachment; filename="%s"' % f.name)
        msg.attach(part2)

        session = smtplib.SMTP('smtp.gmail.com', 587)

        session.ehlo()
        session.starttls()
        session.login('pjeassite@gmail.com', 'EASTasia1!')
        session.sendmail(sender, [recipient], msg.as_string())
    except Error:
        pass
    return True

# for testing only
#send_email(open(sys.argv[1], "rb"))
#pjeaserror.send_error("uhoh")
