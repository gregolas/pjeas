from django.conf.urls import patterns, include, url
from app.views import home, team, about, archive, submit, join, contact, add, upload, load, plogin, plogout, submitarticle, thankyou
from django.views.generic.base import TemplateView, RedirectView

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'prjeas.views.home', name='home'),
    # url(r'^prjeas/', include('prjeas.foo.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^home/$', home),
    url(r'^about/$', about),
    url(r'^archive/$', archive),
    url(r'^submit/$', submit),
    url(r'^team/$', team),
    url(r'^add/$', add),
    url(r'^join/$', join),
    url(r'^upload/$', upload),
    url(r'^contact/$', contact),
    url(r'^plogout/$', plogout),
    url(r'^thankyou/$', thankyou),
    url(r'^plogin/$', plogin),
    url(r'^submitarticle/$', submitarticle),
    url(r'^load/(.*)', load),
    url(r'^$', home),
)
